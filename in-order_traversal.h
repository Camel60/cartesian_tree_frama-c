#include <stddef.h>

#include "predicate.h"

/*@ requires \valid(new_s + (0 .. length - 1));
    requires \valid(father +  (0 .. length - 1));
    requires \valid(side_left +  (0 .. length - 1));
    requires \valid(side_right + (0 .. length - 1));

    requires \separated(new_s + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(new_s + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(new_s + (0 .. length - 1),  father + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(side_right + (0 .. length - 1),  side_left + (0 .. length - 1));

    requires 0 <= root < length;

    assigns new_s[0 .. length-1];

    ensures left_property: \forall size_t i, x; 
       in_tree(side_left[i], father, side_left, side_right, length, x) ==>  0 <= x <= i-1;

    ensures left_property: \forall size_t i, x; 
       in_tree(side_right[i], father, side_left, side_right, length, x) ==> i+1 <= x <= length-1;

 */
void in_order_traversal(int* new_s, size_t root, size_t* side_left, size_t* side_right, size_t* father, size_t length);
