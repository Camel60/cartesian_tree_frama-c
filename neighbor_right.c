#include <stddef.h> // SIZE_T
#include <stdint.h> // SIZE_MAX

/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(stack + (0 .. length - 1));
    requires \valid(right + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1),right + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1), s + (0 .. length - 1));
    requires \separated(right + (0 .. length - 1), s + (0 .. length - 1));
    requires 0 < length < SIZE_MAX;

    assigns stack[0 .. length - 1], right[0 .. length - 1];

    // s'il existe, le voisin droit d'un elt d'indice i a un indice supérieur à i
    ensures e_wf_right: 
      \forall integer i; 1 <= i <= length 
      ==> i < right[i-1] <= length || right[i-1] == length+1;
    // le voisin droit d'un élément est plus petit que cet élément
    ensures e_right_small:
      \forall integer i; 1 <= i <= length
      ==> right[i-1] != length+1
      ==> s[right[i-1]-1] < s[i-1];
    // tous les éléments entre un elt e et son voisin droit (non inclus) sont >= e
    ensures e_right_smallest:
      \forall integer i; 1 <= i <= length
      ==> \forall integer j; i <= j < right[i-1]
      ==> s[j-1] >= s[i-1];
    // // tous les éléments entre un elt e et son voisin droit (non inclus) sont >= e
    // ensures right_smallest_1:
    //   \forall integer i; 1 <= i <= length
    //   ==> right[i-1] != length+1 
    //   ==> \forall integer j; i <= j < right[i-1]
    //   ==> s[j-1] >= s[i-1];
    // // un elt sans voisin droit est plus petit que tous les elts a sa droite
    // ensures right_smallest_2:
    //   \forall integer i; 1 <= i <= length
    //   ==> right[i-1] == length+1
    //   ==> \forall integer j; i <= j <= length
    //   ==> s[j-1] >= s[i-1];
*/
void neighbor_right(int* s, size_t length, size_t* stack, size_t* right) {
  size_t sidx = 0;
  int x = length-1;
  /*@
    loop invariant s_untouched:
			\forall integer idx; 0 <= idx < length 
			==> s[idx] == \at(s[idx],Pre);
    loop invariant y_bounded:
			0 <= y <= length;
    loop invariant x_bounded:
			-1 <= x <= length-1;
    loop invariant sidx_bounded:
			0 <= sidx <= y;
    // la pile ne contient que des indices valides succédant x
    loop invariant stack_right:
      \forall integer i; 0 <= i < sidx 
      ==> x < stack[i]-1 <= length-1;
    // ==ensures mais i va de x+2 à length
    loop invariant wf_right: 
      \forall integer i; x+1 < i <= length 
      ==> i < right[i-1] <= length+1;
      // ==> i < right[i-1] <= length || right[i-1] == length+1;
    // ==ensures mais i va de x+2 à length (x+1 à length-1 en indiçant à 0)
    loop invariant right_small:
      \forall integer i; x+1 <= i <= length-1 
      ==> right[i] != length+1
      ==> s[right[i]-1] < s[i];
    // ==ensures mais i va de x+2 à length
    loop invariant right_smallest:
      \forall integer i; x+1 <= i <= length-1
      ==> \forall integer j; i <= j < right[i]-1
      ==> s[j] >= s[i];
    // // ==ensures mais i va de x+2 à length
    // loop invariant right_smallest_1:
    //   \forall integer i; x+1 < i <= length
    //   ==> right[i-1] != length+1
    //   ==> \forall integer j; i <= j < right[i-1]
    //   ==> s[j-1] >= s[i-1];
  	// // ==ensures mais i va de x+2 à length
    // loop invariant right_smallest_2:
    //   \forall integer i; x+1 < i <= length
    //   ==> right[i-1] == length+1
    //   ==> \forall integer j; i <= j < length
    //   ==> s[j-1] >= s[i-1];
    // les indices dans la pile sont strictement décroissants
  	loop invariant stack_order:
      \forall integer i, j; 0 <= i < j < sidx 
      ==> stack[j] > stack[i] >= length+1;
    // les elements associés aux indices dans le stack sont strictement décroissants 
  	loop invariant stack_sorder:
      \forall integer i, j; 0 <= i < j < sidx 
      ==> s[stack[i]-1] < s[stack[j]-1];
    // l'elt du premier indice de la pile est <= que tous les elts d'indice >=
    loop invariant s_begin:
      sidx > 0 
      ==> \forall integer i; stack[0]-1 <= i <= length-1 
      ==> s[i] >= s[stack[0] - 1];
    // apres le premier tour la pile n'est pas vide et x est au sommet de la pile
  	loop invariant step_n:
      x < length-1 
      ==> sidx > 0 && stack[sidx - 1] == x+2;
    // les elts entre 2 idx successifs du stack sont >= à l'elt indicé par le 1er idx
    loop invariant stack_summary:
      \forall integer i; 0 <= i < sidx - 1 
      ==> \forall integer j;  stack[i+1]-1 < j < stack[i]-1 
      ==> s[j] >= s[stack[i+1]-1];
    // si la pile n'est pas vide alors x est au sommet de la pile
  	loop invariant stack_push: 
      sidx > 0 ==> stack[sidx-1] == x+2;
    loop assigns x, y, sidx, stack[0 .. length - 1], right[0 .. length - 1];
    loop variant length - y;
   */
  for (size_t y = 0; y < length; y++) {
    //x = length-1-y; 
    /*@
    	loop invariant s_untouched_inner:
        \forall integer idx; 0 <= idx < length 
        ==> s[idx] == \at(s[idx],Pre);
    	loop invariant sidx_decrease:
        0 <= sidx <= \at(sidx,LoopEntry);
      // tous les elts entre l'indice en haut du stack et x sont >= s[x]
    	loop invariant right_bigger:
        sidx > 0 
        ==> \forall integer i; x < i < stack[sidx-1]-1
        ==> s[i] >= s[x];
      // si le stack est vide alors tous les idx > x ont des elts >= s[x]
    	loop invariant stack_empty:
        sidx == 0 
        ==> \forall integer i; x < i <= length-1 
        ==> s[i] >= s[x];
    	loop assigns sidx;
    	loop variant sidx;
    */
    while (sidx > 0 && s[stack[sidx-1]-1] >= s[x]) sidx--;
    if (sidx == 0) {
      right[x] = length+1;
    } else {
      /*@ 
        // verif de right_bigger
        assert head_ok:
          \forall integer i; x < i < stack[sidx-1]-1
          ==> s[i] >= s[x];
      */
      right[x] = stack[sidx - 1];
    }
    /*@ 
    	assert small_a1: right[x] != length+1 ==> s[right[x] - 1] < s[x];
    */
label:
    stack[sidx] = x + 1;
    /*@ 
    	assert s_untouched:
        \forall integer idx; 0 <= idx < length 
        ==> s[idx] == \at(s[idx],Pre);
    */
    //@ assert same: right[x] == \at(right[x], label);
    //@ assert small_a2: right[x] != length+1 ==> s[right[x] - 1] < s[x];
    sidx++;
    x--;
  }
}
