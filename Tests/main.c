#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "cartesian_tree_test.h"

int main() {

  size_t root;
  size_t neighbor_left[9];
  size_t side_left[9];
  size_t neighbor_right[9];
  size_t side_right[9];

  printf("TEST 1: Exemple du sujet\n\n"); // TEST 1

  int s1[9] = {4,7,8,1,2,3,9,5,6};
  printf("Séquence initiale : ");
  print_tab_int(s1, 9);
  root = create_cartesian_tree(s1, neighbor_left, neighbor_right, side_left, side_right, 9);
  printf("Racine : %lu\n", root);
  
  printf("Fils gauche : ");
  print_tab_size_t(side_left, 9);
  
  printf("Fils droit : ");
  print_tab_size_t(side_right, 9);


  printf("\nTEST 2: Séquence croissante\n\n"); // TEST 2

  int s2[9] = {1,2,3,4,5,6,7,8,9};
  printf("Séquence initiale : ");
  print_tab_int(s2, 9);
  root = create_cartesian_tree(s2, neighbor_left, neighbor_right, side_left, side_right, 9);
  printf("Racine : %lu\n", root);
  
  printf("Fils gauche : ");
  print_tab_size_t(side_left, 9);
  
  printf("Fils droit : ");
  print_tab_size_t(side_right, 9);

  printf("\nTEST 3:\n\n");                     // TEST 3

  int s3[9] = {4,5,3,6,2,7,1,8,9};
  printf("Séquence initiale : ");
  print_tab_int(s3, 9);
  root = create_cartesian_tree(s3, neighbor_left, neighbor_right, side_left, side_right, 9);
  printf("Racine : %lu\n", root);
  
  printf("Fils gauche : ");
  print_tab_size_t(side_left, 9);
  
  printf("Fils droit : ");
  print_tab_size_t(side_right, 9);

  printf("\nTEST 4:\n\n");                     // TEST 4

  int s4[9] = {1,2,6,7,8,9,4,3,5};
  printf("Séquence initiale : ");
  print_tab_int(s4, 9);
  root = create_cartesian_tree(s4, neighbor_left, neighbor_right, side_left, side_right, 9);
  printf("Racine : %lu\n", root);
  
  printf("Fils gauche : ");
  print_tab_size_t(side_left, 9);
  
  printf("Fils droit : ");
  print_tab_size_t(side_right, 9);
  
  return 0;
}
