#include <stddef.h>

/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(stack + (0 .. length - 1));
    requires \valid(left + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1),left + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1), s + (0 .. length - 1));
    requires \separated(left + (0 .. length - 1), s + (0 .. length - 1));

    assigns stack[0 .. length - 1], left[0 .. length - 1];

    ensures wf_left: \forall integer i; 0 <= i < length ==> 0 <= left[i] <= i;
    ensures left_small:
    \forall integer i; 0 <= i < length ==> left[i] > 0 ==> s[left[i]-1] < s[i];
     ensures left_smallest:
     \forall integer i; 0 <= i < length ==>
        \forall integer j; left[i] <= j < i ==> s[j] >= s[i];
*/
void create_neighbor_left(int* s, size_t length, size_t* stack, size_t* left);

/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(stack + (0 .. length - 1));
    requires \valid(right + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1), right + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1), s + (0 .. length - 1));
    requires \separated(right + (0 .. length - 1), s + (0 .. length - 1));
    assigns stack[0 .. length - 1], right[0 .. length - 1];
    ensures wf_right: \forall integer i; 0 <= i < length ==> right[i] == 0 || i <= right[i];
    ensures right_small:
    \forall integer i; 0 <= i < length ==> right[i] > 0 ==> s[right[i]-1] < s[i];
     ensures right_smallest:
     \forall integer i; 0 <= i < length ==>
        \forall integer j; i <= j < right[i] ==> s[j] >= s[i];
*/
void create_neighbor_right(int* s, size_t length, size_t* stack, size_t* right);
