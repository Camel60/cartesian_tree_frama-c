#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "neighbors.h"
#include "in-order_traversal.h"


/* Affichage d'un tableau */
void print_tab_size_t(size_t* tab, size_t length){
  
  int i;
  printf("[");
  for(i = 0 ; i < length-1 ; i++){
    printf("%lu,", tab[i]);
  }
  printf("%lu]\n", tab[length-1]);
  
}

void print_tab_int(int* tab, size_t length){
  
  int i;
  printf("[");
  for(i = 0 ; i < length-1 ; i++){
    printf("%d,", tab[i]);
  }
  printf("%d]\n", tab[length-1]);
  
}

/*
   @requires: la séquence contient uniquement des nombres distints
   @assumes
   @ensures: un arbre cartésien */


//    ensures order: \forall integer i; in_order_traversal(\result, side_left, side_right, length)[i] == i;


//    ensures wf_binary_tree: \forall integer x; In x s <==> "In" x \result;
//    ensures wf_binary_tree_x: \forall integer x, i; s[i] == x ==> (in_sequence x s <==> in_tree i \result);

//    \forall integer x; "In" x \result ==>
//                               (x->left != NULL ==> (x->left)->value > x)
//                            && (x->right != NULL ==> (x->right)->value > x);
/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(neighbor_left +  (0 .. length - 1));
    requires \valid(neighbor_right + (0 .. length - 1));
    requires \valid(side_left +  (0 .. length - 1));
    requires \valid(side_right + (0 .. length - 1));

    requires \separated(neighbor_left + (0 .. length - 1),  neighbor_right + (0 .. length - 1));
    requires \separated(neighbor_left + (0 .. length - 1),  s + (0 .. length - 1));
    requires \separated(neighbor_left + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(neighbor_left + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(neighbor_right + (0 .. length - 1), s + (0 .. length - 1));
    requires \separated(neighbor_right + (0 .. length - 1), side_right + (0 .. length - 1));
    requires \separated(neighbor_right + (0 .. length - 1), side_left + (0 .. length - 1));
    requires \separated(s + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(s + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(side_right + (0 .. length - 1),  side_left + (0 .. length - 1));

    requires \forall integer i, j; i != j ==> s[i] != s[j];
    

    assigns neighbor_left[0 .. length-1], neighbor_right[0 .. length-1], side_left[0 .. length-1], side_right[0 .. length-1];


    ensures wf_binary_tree_in: \forall size_t i; 0 <= i < length <==> in_tree(\result, side_left, side_right, length, i);

    ensures wf_binary_tree_without_cycle_side_left: 
      \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==>
      side_left[i] != length ==> side_left[j] != length ==> 
      side_left[i] != side_left[j];
    ensures wf_binary_tree_without_cycle_side_right: 
      \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==>
      side_right[i] != length ==> side_right[j] != length ==> 
      side_right[i] != side_right[j];
    ensures wf_binary_tree_without_cycle_side_right_side_left: 
      \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==>
      side_right[i] != length ==> side_left[j] != length ==> 
      side_right[i] != side_left[j];            
    ensures wf_binary_tree_without_cycle_root_side_left: 
      \forall integer i; 0 <= i < length ==>
      side_left[i] != length ==> 
      side_left[i] != \result;
    ensures wf_binary_tree_without_cycle_root_side_right: 
      \forall integer i; 0 <= i < length ==>
      side_right[i] != length ==> 
      side_right[i] != \result;

 
    ensures heap_property_left:  
      \forall integer i; 0 <= i < length ==> 
      side_left[i] != length ==>
      s[side_left[i]] > s[i];
    ensures heap_property_right:  
      \forall integer i; 0 <= i < length ==> 
      side_right[i] != length ==>
      s[side_right[i]] > s[i];
*/
size_t create_cartesian_tree(int* s, size_t* neighbor_left, size_t* neighbor_right, size_t* side_left, size_t* side_right, size_t length) {
  
  /**
     PART A: Construction de la séquence de voisins gauches */
  size_t stack_left[length];
  create_neighbor_left(s, length, stack_left, neighbor_left);
  printf("Voisins de gauche : ");
  print_tab_size_t(neighbor_left, length);

   /**
     PART B: Construction de la séquence de voisins droits */
  size_t stack_right[length];
  create_neighbor_right(s, length, stack_right, neighbor_right);
  printf("Voisins de droite : ");
  print_tab_size_t(neighbor_right, length);

  /**
     PART C: Construction de l'arbre cartésien */

  // STEP 0: Initialisation
  constant_vector(side_left,  length, length);
  constant_vector(side_right, length, length);

  size_t always_seen[length];
  constant_vector(always_seen, length, 0);

  size_t i_son;
  size_t i_father;
  size_t i_root;

  size_t father[length];

/*@ loop invariant s_untouched:
        \forall integer idx; 0 <= idx < length ==> s[idx] == \at(s[idx],Pre);
    loop invariant 0 <= i_son <= length;
    loop invariant 0 <= i_father < length;
    loop invariant 0 <= i_root < length;
    loop invariant \forall integer j; j < i_son ==> always_seen[j] == 1;
    loop invariant \forall integer j; j > i_son ==> always_seen[j] == 0;

    
    loop assigns i_son, i_father, i_root, side_left[0 .. length-1], side_right[0 .. length-1], always_seen[0 .. length-1];
    loop variant length - i_son;
   */
  for (i_son=0 ; i_son < length ; i_son++){
    
    if (neighbor_left[i_son] > 0 && neighbor_right[i_son] == 0) {
      //printf("1: %d\n", i_son);
      i_father = neighbor_left[i_son]-1;
      update(i_son, i_father, side_left, side_right, always_seen, length);
      father[i_son] = i_father;
      
    } else if (neighbor_right[i_son] > 0 && neighbor_left[i_son] == 0) {
      //printf("2: %d\n", i_son);
      i_father = neighbor_right[i_son]-1;
      update(i_son, i_father, side_left, side_right, always_seen, length);
      father[i_son] = i_father;
      
    } else if (neighbor_left[i_son] > 0 && neighbor_right[i_son] > 0) {
      //printf("3: %d\n", i_son);
      i_father = maximum_position(s, neighbor_right[i_son]-1, neighbor_left[i_son]-1, length);
      update(i_son, i_father, side_left, side_right, always_seen, length);
      father[i_son] = i_father;
      
    } else {
      //printf("4: %d\n", i_son);
      i_root = i_son;
      father[i_son] = length;
    }
    always_seen[i_son] = 1;
  }
  
  printf("C'est qui ton père ? ");
  print_tab_size_t(father, length);
  
  // ensures: forall integer i; \result[i] == i; ensures order ???
  // in_order_traversal(i_root, side_left, side_right, length);  // s ?
  int new_s[length];
  in_order_traversal(new_s, i_root, side_left, side_right, father, length);
  printf("Dans le bon ordre ? ");
  print_tab_int(new_s, length);
  return i_root;
}

