#include <stddef.h>

/*@
     predicate in_sequence(int* sequence, integer from, integer to, int v) = \exists integer i; from <= i <= to ==> sequence[i] == v;

     predicate in(size_t value, size_t* array, size_t length) = \exists integer i; 0 <= i < length ==> array[i] == value;
 
     predicate in_tree(size_t root, size_t* father, size_t* side_left, size_t* side_right, size_t length, size_t v) =
         v < length && (in(v, side_left, length) || in(v, side_right, length) || v == root);

     predicate is_minimum(int* s, integer length, integer i) =
         0 <= i < length &&
         (\forall integer k; 0 <= k < length ==> s[i] <= s[k]);

*/
