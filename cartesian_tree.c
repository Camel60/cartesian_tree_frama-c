#include <stddef.h>
#include <stdint.h>

#include "predicate.h"
#include "prelude.h"
#include "neighbors.h"


   
/*@ lemma have_minimum: \forall int* s; \forall  integer length; 
                       (\forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==> s[i] != s[j]) ==>
          \exists integer k; 0 <= k < length ==> (\forall integer i; 0 <= i < length ==> i != k ==> s[k] < s[i]); */
/*@ lemma unique_minimum: \forall int* s; \forall  integer length; 
        (\forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==> s[i] != s[j]) ==>
         \forall integer i, j; is_minimum(s,length,i) ==> is_minimum(s,length,j) ==> i == j; */


/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(father +  (0 .. length - 1));
    requires \valid(side_left +  (0 .. length - 1));
    requires \valid(side_right + (0 .. length - 1));

    requires \separated(father + (0 .. length - 1),  s + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(s + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(s + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(side_right + (0 .. length - 1),  side_left + (0 .. length - 1));

    requires \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==> s[i] != s[j];

    assigns father[0 .. length-1], side_left[0 .. length-1], side_right[0 .. length-1];

    behavior all_values:
      ensures wf_BT_in_tree_to_s: 
          \forall size_t i; in_tree(\result, father, side_left, side_right, length, i) ==> 0 <= i < length;
      ensures wf_BT_in_s_to_tree: 
          \forall size_t i; 0 <= i < length ==> in_tree(\result, father, side_left, side_right, length, i);

    behavior prop_root:
      ensures wf_BT_root:        \result != length;
      ensures wf_BT_father_root: father[\result] == length;
      ensures wf_BT_unique_root: \forall integer i; 0 <= i < length ==> i != \result ==> father[i] != length;

    behavior cycle:
      ensures wf_BT_without_cycle_side_left: 
        \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
        side_left[i] != length ==> side_left[j] != length ==>
        side_left[i] != side_left[j];
      ensures wf_BT_without_cycle_side_right: 
        \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
        side_right[i] != length ==> side_right[j] != length ==> 
        side_right[i] != side_right[j];
      ensures wf_BT_without_cycle_side_right_side_left: 
        \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
        side_right[i] != length ==> side_left[j] != length ==> 
        side_right[i] != side_left[j];            
      ensures wf_BT_without_cycle_root_side_left: 
        \forall integer i; 0 <= i < length ==>
        side_left[i] != length ==> 
        side_left[i] != \result;
      ensures wf_BT_without_cycle_root_side_right: 
        \forall integer i; 0 <= i < length ==>
        side_right[i] != length ==> 
        side_right[i] != \result;

    behavior prop_father: 
      ensures wf_BT_father_left:  
          \forall size_t j; 0 <= j < length ==> father[j] != length ==> side_left[father[j]] != length ==> side_left[father[j]] == j;
      ensures wf_BT_father_right: 
          \forall size_t j; 0 <= j < length ==> father[j] != length ==> side_right[father[j]] != length ==> side_right[father[j]] == j;

    behavior prop_heap:  
      ensures heap_property_father: 
        \forall integer i; 0 <= i < length ==> father[i] != length ==> s[father[i]] < s[i];

*/
size_t create_cartesian_tree(int* s, size_t* father, size_t* side_left, size_t* side_right, size_t length) {
  
  /** PART A: Construction de la séquence de voisins gauches */
  size_t stack_left[SIZE_MAX];
  size_t neighbor_left[SIZE_MAX];
  create_neighbor_left(s, length, stack_left, neighbor_left);
  /*@ assert neighbor_left_values: \forall integer i; 0 <= i < length ==> 0 <= neighbor_left[i] < length; */

  /** PART B: Construction de la séquence de voisins droits */
  size_t stack_right[SIZE_MAX];
  size_t neighbor_right[SIZE_MAX];
  create_neighbor_right(s, length, stack_right, neighbor_right);
  /*@ assert neighbor_right_values: \forall integer i; 0 <= i < length ==> 0 <= neighbor_right[i] < length; */
  
  /*@ assert prop_minimum: \exists integer k; 0 <= k < length ==> 
         (\forall integer i; 0 <= i < length ==> i != k ==> s[k] < s[i] <==>
         neighbor_left[k] == 0 && neighbor_right[k] == 0); */
  /*@ assert prop_minimum_2: \exists integer k; 0 <= k < length ==> 
         (is_minimum(s, length, k) <==>
         neighbor_left[k] == 0 && neighbor_right[k] == 0); */
label:

  /** PART C: Construction de l'arbre cartésien */

  // STEP 1: Initialisation
  constant_vector(side_left,  length, length);
  
  constant_vector(side_right, length, length);
  

  size_t always_seen[SIZE_MAX];
  constant_vector(always_seen, length, 0);

  size_t i_son;
  size_t i_father=length;
  size_t i_root=length;

  // STEP 2: Build on cartesian tree
/*@    
    loop invariant neighbor_left_untouched:
        \forall integer idx; 0 <= idx < length ==> neighbor_left[idx] == \at(neighbor_left[idx], label);
    loop invariant neighbor_right_untouched:
        \forall integer idx; 0 <= idx < length ==> neighbor_right[idx] == \at(neighbor_right[idx], label);
    
    loop invariant i_son_values:       0 <= i_son <= length;
    loop invariant i_father_values:    0 <= i_father <= length;
    loop invariant i_root_values:      0 <= i_root <= length;
    loop invariant father_values:      \forall integer j; 0 <= j < i_son ==> 0 <= father[j] <= length;
    loop invariant always_seen_values: \forall integer i; 0 <= i < length ==> always_seen[i] == 0 || always_seen[i] == 1;
    loop invariant always_seen_1:      \forall integer j; 0 <= j < i_son ==> always_seen[j] == 1;
    loop invariant always_seen_0:      \forall integer j; i_son < j < length ==> always_seen[j] == 0;
    loop invariant side_left_values:   \forall integer j; 0 <= j < length ==> 0 <= side_left[j] <= length;
    loop invariant side_right_values:  \forall integer j; 0 <= j < length ==> 0 <= side_right[j] <= length;

    loop invariant i_root_prop: 
          \forall integer k; 0 <= k < i_son ==> (neighbor_left[k] == 0 && neighbor_right[k] == 0 <==> k == i_root);

    loop invariant i_root_untouched: 
          \forall integer j; (neighbor_left[\at(i_son, LoopCurrent)] > 0 || neighbor_right[\at(i_son, LoopCurrent)] > 0) ==> 
          i_root == \at(i_root, LoopCurrent);

    loop assigns i_son, i_father, i_root, father[0..length-1], side_left[0..length-1], side_right[0..length-1], always_seen[0..length-1];

    for prop_root: loop invariant wf_BT_unique_root: 
          \forall integer j; 0 <= j < i_son ==> j != i_root ==> father[j] != length;
    for prop_root: loop invariant wf_BT_father_root: i_root != length ==> father[i_root] == length;

    for cycle: loop invariant wf_BT_strict_left: \forall integer j; 0 <= j < length ==> 
            i_father != length ==> j != i_father ==> side_left[j] != length ==> side_left[j] < i_son;   
    for cycle: loop invariant wf_BT_strict_right: \forall integer j; 0 <= j < length ==> 
            i_father != length ==> j != i_father ==> side_right[j] != length ==> side_right[j] < i_son;

    for cycle: loop invariant wf_BT_without_cycle_side_left: 
      \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
      side_left[i] != length ==> side_left[j] != length ==>
      side_left[i] != side_left[j];
    for cycle: loop invariant wf_BT_without_cycle_side_right: 
      \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
      side_right[i] != length ==> side_right[j] != length ==> 
      side_right[i] != side_right[j];
    for cycle: loop invariant wf_BT_without_cycle_side_right_side_left: 
      \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
      side_right[i] != length ==> side_left[j] != length ==> 
      side_right[i] != side_left[j];
    for cycle: loop invariant wf_BT_without_cycle_root_side_left: 
      \forall integer i; 0 <= i < length ==>
      side_left[i] != length ==> 
      side_left[i] != i_root;
    for cycle: loop invariant wf_BT_without_cycle_root_side_right: 
      \forall integer i; 0 <= i < length ==>
      side_right[i] != length ==> 
      side_right[i] != i_root;

    for prop_father: loop invariant wf_BT_father_left:  
          \forall size_t j; 0 <= j < i_son ==> father[j] != length ==> side_left[father[j]] != length ==> side_left[father[j]] == j;
    for prop_father: loop invariant wf_BT_father_right: 
          \forall size_t j; 0 <= j < i_son ==> father[j] != length ==> side_right[father[j]] != length ==> side_right[father[j]] == j;

    for prop_heap: loop invariant heap_property: 
          \forall integer j; 0 <= j < i_son ==> father[j] != length ==> s[father[j]] < s[j];

    loop variant length - i_son;
*/
  for (i_son=0 ; i_son < length ; i_son++){

    if (neighbor_left[i_son] > 0 && neighbor_right[i_son] == 0) {
      i_father = neighbor_left[i_son]-1;
      /*@ assert case_1: s[i_son] > s[i_father] && i_son > i_father; */
      /*@ assert case_1_left:  \forall integer i; i_father < i < i_son ==> s[i_son] < s[i]; */
      /*@ assert case_1_right: \forall integer i; i_son < i < length   ==> s[i_son] < s[i]; */
      update(i_son, i_father, side_left, side_right, always_seen, length);
      father[i_son] = i_father;
      
    } else if (neighbor_right[i_son] > 0 && neighbor_left[i_son] == 0) {
      i_father = neighbor_right[i_son]-1;
      /*@ assert case_2: s[i_son] > s[i_father] && i_son < i_father; */
      /*@ assert case_2_left:  \forall integer i; 0 <= i < i_son ==> s[i_son] < s[i];       */
      /*@ assert case_2_right: \forall integer i; i_son < i < i_father ==> s[i_son] < s[i]; */
      update(i_son, i_father, side_left, side_right, always_seen, length);
      father[i_son] = i_father;
      
    } else if (neighbor_left[i_son] > 0 && neighbor_right[i_son] > 0) {
      i_father = maximum_position(s, neighbor_right[i_son]-1, neighbor_left[i_son]-1, length);
      /*@ assert case_3: s[i_son] > s[i_father]; */
      /*@ assert case_3_left:  \forall integer i; i_father < i < i_son ==> s[i_son] < s[i]; */
      /*@ assert case_3_right: \forall integer i; i_son < i < i_father ==> s[i_son] < s[i]; */
      update(i_son, i_father, side_left, side_right, always_seen, length);
      father[i_son] = i_father;
      
    } else {
      i_root = i_son;
      /*@ assert case_4_left:  \forall integer j; 0 <= j < i_son ==> s[i_son] < s[j]; */
      /*@ assert case_4_right: \forall integer j; i_son < j < length ==> s[i_son] < s[j]; */
      father[i_son] = length;
    }
    /*@ assert side_left_touched: 
           i_father != length ==> (neighbor_left[i_son] > 0 || neighbor_right[i_son] > 0) ==> always_seen[i_father] == 0 ==> 
                                side_left[i_father]  == i_son && side_right[i_father] == \at(side_right[i_father], LoopCurrent)
                                && i_root == \at(i_root, LoopCurrent); */

    /*@ assert i_root_touched_1: 
           i_father != length ==> (neighbor_left[i_son] == 0 && neighbor_right[i_son] == 0) ==>
                                i_root == i_son && 
                                side_right[i_father] == \at(side_right[i_father], LoopCurrent) && 
                                side_left[i_father]  == \at(side_left[i_father], LoopCurrent);  */
    /*@ assert i_root_touched_2: i_father == length ==> i_root == i_son; */
    
    /*@ assert i_root_prop_2: i_root != length <==> neighbor_left[i_root] == 0 && neighbor_right[i_root] == 0; */
    always_seen[i_son] = 1;
    /*@ assert summarize_touched:
           (i_father != length ==>
               (side_left[i_father]  == i_son
                && side_left[i_father] != \at(side_left[i_father], LoopCurrent)
                && side_right[i_father] == \at(side_right[i_father], LoopCurrent)
                && i_root == \at(i_root, LoopCurrent))
      ) || (i_father != length ==>
               (side_right[i_father] == i_son 
                && side_right[i_father] != \at(side_right[i_father], LoopCurrent)
                && side_left[i_father]  == \at(side_left[i_father], LoopCurrent)
                && i_root == \at(i_root, LoopCurrent))
      ) || (i_father != length ==>
               (i_root == i_son
                && i_root != \at(i_root, LoopCurrent) 
                && side_right[i_father] == \at(side_right[i_father], LoopCurrent) 
                && side_left[i_father]  == \at(side_left[i_father], LoopCurrent))
      ) || (i_father == length ==> 
               (i_root == i_son
	       && i_root != \at(i_root, LoopCurrent))); */

    
    /*@ assert father_part_untouched: 
         \forall integer j; 0 <= j < length ==> j != i_son ==> father[j] == \at(father[j], LoopCurrent); */
    /*@ assert side_left_part_untouched:
         \forall integer j; 0 <= j < length ==> j != i_father ==> side_left[j] == \at(side_left[j], LoopCurrent); */
    /*@ assert side_right_part_untouched:
         \forall integer j; 0 <= j < length ==> j != i_father ==> side_right[j] == \at(side_right[j], LoopCurrent); */
    /*@ assert always_seen_part_untouched: 
         \forall integer j; 0 <= j < length ==> j != i_son ==> always_seen[j] == \at(always_seen[j], LoopCurrent); */

    // Ne fonctionnent pas
    /*  assert changed_i_root: i_father != length ==>
                                (i_root == i_son <==> side_left[i_father] == \at(side_left[i_father], LoopCurrent) && 
                                                    side_right[i_father] == \at(side_right[i_father], LoopCurrent));    */
    /*  assert changed_side_left: i_father != length ==> 
              (side_left[i_father] != \at(side_left[i_father], LoopCurrent)
                  <==>
               side_left[i_father] == i_son 
               && i_root == \at(i_root, LoopCurrent) 
               && side_right[i_father] == \at(side_right[i_father], LoopCurrent));    */
    /*  assert changed_side_left: i_father != length ==> (side_left[i_father] == i_son <==> i_root == \at(i_root, LoopCurrent) && 
                                                    side_right[i_father] == \at(side_right[i_father], LoopCurrent));    */
    /*  assert changed_side_right: i_father != length ==> (side_right[i_father] == i_son <==> i_root == \at(i_root, LoopCurrent) && 
                                                    side_left[i_father] == \at(side_left[i_father], LoopCurrent));    */

    
  }
  /*@ assert i_root_at_the_end: i_root != length; */
  return i_root;
}
