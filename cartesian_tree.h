#include <stddef.h>

#include "predicate.h"

/*@ lemma have_minimum: \forall int* s; \forall  integer length; 
                       (\forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==> s[i] != s[j]) ==>
          \exists integer k; 0 <= k < length ==> (\forall integer i; 0 <= i < length ==> i != k ==> s[k] < s[i]); */
/*@ lemma unique_minimum: \forall int* s; \forall  integer length; 
        (\forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==> s[i] != s[j]) ==>
         \forall integer i, j; is_minimum(s,length,i) ==> is_minimum(s,length,j) ==> i == j; */


/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(father +  (0 .. length - 1));
    requires \valid(side_left +  (0 .. length - 1));
    requires \valid(side_right + (0 .. length - 1));

    requires \separated(father + (0 .. length - 1),  s + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(s + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(s + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(side_right + (0 .. length - 1),  side_left + (0 .. length - 1));

    requires \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==> s[i] != s[j];

    assigns father[0 .. length-1], side_left[0 .. length-1], side_right[0 .. length-1];

    behavior all_values:
      ensures wf_BT_in_tree_to_s: 
          \forall size_t i; in_tree(\result, father, side_left, side_right, length, i) ==> 0 <= i < length;
      ensures wf_BT_in_s_to_tree: 
          \forall size_t i; 0 <= i < length ==> in_tree(\result, father, side_left, side_right, length, i);

    behavior prop_root:
      ensures wf_BT_root:        \result != length;
      ensures wf_BT_father_root: father[\result] == length;
      ensures wf_BT_unique_root: \forall integer i; 0 <= i < length ==> i != \result ==> father[i] != length;

    behavior cycle:
      ensures wf_BT_without_cycle_side_left: 
        \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
        side_left[i] != length ==> side_left[j] != length ==>
        side_left[i] != side_left[j];
      ensures wf_BT_without_cycle_side_right: 
        \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
        side_right[i] != length ==> side_right[j] != length ==> 
        side_right[i] != side_right[j];
      ensures wf_BT_without_cycle_side_right_side_left: 
        \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==>
        side_right[i] != length ==> side_left[j] != length ==> 
        side_right[i] != side_left[j];            
      ensures wf_BT_without_cycle_root_side_left: 
        \forall integer i; 0 <= i < length ==>
        side_left[i] != length ==> 
        side_left[i] != \result;
      ensures wf_BT_without_cycle_root_side_right: 
        \forall integer i; 0 <= i < length ==>
        side_right[i] != length ==> 
        side_right[i] != \result;

    behavior prop_father: 
      ensures wf_BT_father_left:  
          \forall size_t j; 0 <= j < length ==> father[j] != length ==> side_left[father[j]] != length ==> side_left[father[j]] == j;
      ensures wf_BT_father_right: 
          \forall size_t j; 0 <= j < length ==> father[j] != length ==> side_right[father[j]] != length ==> side_right[father[j]] == j;

    behavior prop_heap:  
      ensures heap_property_father: 
        \forall integer i; 0 <= i < length ==> father[i] != length ==> s[father[i]] < s[i];

*/
size_t create_cartesian_tree(int* s, size_t* father, size_t* side_left, size_t* side_right, size_t length);
