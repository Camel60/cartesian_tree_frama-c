#include <stddef.h>

/*@ requires \valid(s + (0 .. length - 1));
    requires 0 <= a < length;
    requires 0 <= b < length;

    assigns \nothing;

    ensures s[\result] >= s[a];
    ensures s[\result] >= s[b];

    behavior stric_inf:
	assumes s[a] < s[b];
        ensures \result == b;
    behavior sup:
	assumes s[a] >= s[b];
        ensures \result == a;
    complete behaviors; disjoint behaviors;
*/
size_t maximum_position(int* s, size_t a, size_t b, size_t length);



/*@ requires \valid(tab + (0 .. length-1));
    assigns tab[0 .. length-1];
    ensures \forall integer i; 0 <= i < length ==> tab[i] == value;
*/
void constant_vector(size_t* tab, size_t length, size_t value);



/*@ requires \valid(side_left + (0 .. length-1));
    requires \valid(side_right + (0 .. length-1));
    requires \valid(always_seen + (0 .. length-1));
    requires \separated(side_left + (0 .. length-1),  side_right + (0 .. length-1));
    requires \separated(side_left + (0 .. length-1),  always_seen + (0 .. length-1));
    requires \separated(side_right + (0 .. length-1), always_seen + (0 .. length-1));
    
    requires i_father_values: 0 <= i_father < length;
    requires i_son_values:    0 <= i_son < length;
    requires \forall integer i; 0 <= i < length ==> always_seen[i] == 0 || always_seen[i] == 1;

    assigns side_left[i_father], side_right[i_father];

    behavior always_seen:
	assumes always_seen[i_father] == 1;
	ensures side_right[i_father] == i_son && side_left[i_father] == \at(side_left[i_father], Pre);
    behavior never_seen:
	assumes always_seen[i_father] == 0;
	ensures side_left[i_father] == i_son && side_right[i_father] == \at(side_right[i_father], Pre);
    complete behaviors; disjoint behaviors;
*/
void update(size_t i_son, size_t i_father, size_t* side_left, size_t* side_right, size_t* always_seen, size_t length);
