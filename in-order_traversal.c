#include <stddef.h>
#include <stdint.h>

#include "prelude.h"
/*
  logic integer factorial(integer n) = (n <= 0) ? 1 : n * factorial(n-1);
*/
/*@
  logic integer depth_aux(integer i, integer i_node, integer i_root, size_t* father, size_t* side_left, size_t* side_right, integer length)
            = (i_node != i_root && i < length) ? i : depth_aux(i+1, father[i_node], i_root, father, side_left, side_right, length);

  logic integer depth(integer i_node, integer i_root, size_t* father, size_t* side_left, size_t* side_right, integer length) 
            = depth_aux(0, i_node, i_root, father, side_left, side_right, length);
 */
/*@
     axiomatic OccArraySize_t{
	logic integer occ(size_t e, size_t* array, integer from, integer to)
	   reads array[from .. (to-1)];
	axiom end_occ:
	   \forall size_t e, size_t* array, integer from, to;
               from >= to ==> occ(e, array, from, to) == 0;
	axiom iter_occ_true:
	   \forall size_t e, size_t* array, integer from, to;
               (from < to && array[to-1] == e) ==> 
               occ(e, array, from, to) == occ(e, array, from, to-1) + 1;
	axiom iter_occ_false:
	   \forall size_t e, size_t* array, integer from, to;
               (from < to && array[to-1] != e) ==> 
               occ(e, array, from, to) == occ(e, array, from, to-1);
     }
     logic integer nb_not_seen(size_t* array, integer length) = occ((unsigned int)0, array, 0, length); 
*/

/*@ requires \valid(new_s + (0 .. length - 1));
    requires \valid(father +  (0 .. length - 1));
    requires \valid(side_left +  (0 .. length - 1));
    requires \valid(side_right + (0 .. length - 1));

    requires \separated(new_s + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(new_s + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(new_s + (0 .. length - 1),  father + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_left + (0 .. length - 1));
    requires \separated(father + (0 .. length - 1),  side_right + (0 .. length - 1));
    requires \separated(side_right + (0 .. length - 1),  side_left + (0 .. length - 1));

    requires 0 < length;
    requires 0 <= root < length;
    requires \forall integer i; 0 <= i < length ==> 0 <= father[i]     <= length;
    requires \forall integer i; 0 <= i < length ==> 0 <= side_left[i]  <= length;
    requires \forall integer i; 0 <= i < length ==> 0 <= side_right[i] <= length;

    assigns new_s[0 .. length-1];

    ensures left_property: \forall size_t i, x; 
       in_tree(side_left[i], father, side_left, side_right, length, x) ==>  0 <= x <= i-1;

    ensures right_property: \forall size_t i, x; 
       in_tree(side_right[i], father, side_left, side_right, length, x) ==> i+1 <= x <= length-1;

 */
void in_order_traversal(size_t* new_s, size_t root, size_t* side_left, size_t* side_right, size_t* father, size_t length) {

  size_t i = 0;
  size_t start = root;
  size_t always_seen[SIZE_MAX];
  constant_vector(always_seen, length, 0);
  /*
      loop invariant i_values:      0 <= i <= length;
      loop invariant occ_values:    0 <= nb_not_seen(always_seen, length) <= length;
      loop invariant occ_i_egality: nb_not_seen(always_seen, length) + i == length;
      loop variant nb_not_seen(always_seen, length);
  */
  /*@ 
  
      loop invariant i_values:           0 <= i <= length;
      loop invariant start_values:       0 <= start < length;
      loop invariant new_s_values:       \forall integer j; 0 <= j < i ==> 0 <= new_s[j] < length;

      loop invariant always_seen_values: \forall integer j; 0 <= j < length ==> always_seen[j] == 0 || always_seen[j] == 1;

      loop assigns i, start, new_s[0..length-1], always_seen[0..length-1];
      loop variant nb_not_seen(&always_seen[0], length);
  */
// loop invariant always_seen_values: \forall integer j; 0 <= j < i ==> always_seen[j] == 0 || always_seen[j] == 1; FAUX
  while ((always_seen[start] == 0)||(i < length)) {
    
    /*@
        loop invariant start_values: 0 <= start < length;

        loop assigns left: start;
    */
    //  loop variant length - depth(father, root, father, side_left, side_right, length);
    while (side_left[start] < length && always_seen[side_left[start]] == 0) {    /* Nous allons tout à gauche */
      start = side_left[start];
    }
    new_s[i] = start;                      /* Nous stockons la valeur trouvé */
update_new_s:    
    always_seen[start] = 1;
update_always_seen:
    i++;
    if (side_right[start] < length) {      /* Nous allons à droite, et on recommence */
      start = side_right[start]; 
    } else {                               /* Sinon, nous remontons jusqu'au père non vu */
      start = father[start];
      /*@ assert father[root] == length; */

      /*@ 
          
          loop assigns up: start;
          loop invariant start_values: 0 <= start < length;

          loop invariant always_seen_left:
                 always_seen[start] == 1 ==>
                 \forall size_t x; in_tree(side_left[start], father, side_left, side_right, length, x) ==> always_seen[x] == 1;
          loop invariant always_seen_right:
                 always_seen[start] == 0 ==>
                 \forall size_t x; in_tree(side_right[start], father, side_left, side_right, length, x) ==> always_seen[x] == 0;

      */ /*
          loop variant depth(start, root, father, side_left, side_right, length);
      */
      while (always_seen[start] == 1 && start != root) {
	start = father[start];
      }
    }
    /*@ assert new_s_untouched: 
            \forall integer idx; 0 <= idx < length ==> new_s[idx] == \at(new_s[idx], update_new_s); */
    /*@ assert always_seen_untouched: 
            \forall integer idx; 0 <= idx < length ==> always_seen[idx] == \at(always_seen[idx], update_always_seen); */
  }
}
