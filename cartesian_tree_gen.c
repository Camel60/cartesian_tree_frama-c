#include <stddef.h>
#include <stdint.h>

#include "prelude.h"
#include "in-order_traversal.h"
#include "cartesian_tree.h"

/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(new_s +  (0 .. length - 1));
    requires \separated(s + (0 .. length - 1),  new_s + (0 .. length - 1));

    requires \forall integer i, j; 0 <= i < length ==> 0 <= j < length ==> i != j ==> s[i] != s[j];
    
    assigns new_s[0 .. length-1];

    ensures order: \forall integer i; new_s[i] == i;
*/
void cartesian_tree_gen(int* s, int* new_s, size_t length){

  size_t father[SIZE_MAX];
  size_t side_left[SIZE_MAX];
  size_t side_right[SIZE_MAX];
  
  size_t i_root = create_cartesian_tree(s, father, side_left, side_right, length);
  in_order_traversal(new_s, i_root, side_left, side_right, father, length);
}
